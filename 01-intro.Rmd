# Introduction {#intro}

This book is dedicated to the use of R for analyzing climate data. All of our examples will be drawn from climate science, however, most of these skills can be applied to the analysis of other time series. In fact, much of this manual was inspired by an introductory manual on time series analysis in R by @zucchini_time_2003. As such, the information you learn in this book can be used across scientific disciplines.  

## What is a climate time series?

Before we proceed, you might be asking yourself, what is a time series? When we discuss a time series, we are referring to set of observations (of one or more variables of interest) taken over time. These observations may be collected continuously, or might be collected at fixed measurement intervals. A time series may be continuous or it may be discrete, but the method of measurement should be consistent over time. If your data set has a timestamp associated with each observation, you're probably working with a time series!

When we refer to a climate time series, we are usually referring to a series of observations of temperature, precipitation, pressure, humidity, soil moisture, wind speed and direction, and/or incoming solar radiation. 

Our observations of a climate variable will reflect the state of the atmosphere when the observation was made. In this sense, we use observed climate data to better understand the mechanism of these processes. Indeed, in climate science we use retrospection, examining the past to replicate the present, which allows us to better understand climate processes when we make forecasts into the future. 

## Why do we analyze time series data?

Our analysis of time series allows us to better comprehend the mechanisms that underlie our past observations, such that we can forecast into the future. Our first step is to find a model that fits our data. In general, we want to reduce our time series to “random noise”. Why? When we have just random noise remaining, we can assume that our model has captured all of the "predictable" aspects of our time series, as such, we can use that model to generate forecasts^[Resist the urge to use the word "prediction" here. In climate modelling, we are not predicting the future, but rather making a forecast based on our current understanding of observed data and the Earth's climate system.] of the future.

## Steps to time series analysis

In general, you will likely approach most of your time-series based analysis using a few simple steps. 

We start with the _visualization_ of the data. Some of the key tools used in this step include histograms, quantile&ndash;quantile plots, and scatter plots, among other tools. 

By visualizing the data, we can begin to _identify_ characteristics of the time series in the plot. For instance, do we see a trend? Does this trend affect the mean, the variance, or both? Do we see seasonality? What is the period?^[Readers in North America might take for granted that there are four seasons in the year, but in locales nearer to the equator, there are usually only two, and even then this seasonality is usually only apparent in the precipitation signal, not in the temperature.] Do we see any other variability that we can't explain from seasonality?

In some cases, our data may require additional _processing_. Note that "processing" does not mean "adjusting our data until it shows the signal we're looking for", but rather adjusting our data to make our analysis more statistically robust. Some examples of processing include log or square root transformations. 

When we make forecasts, we work on the assumption that our observed data set is stationary^[Stationarity implies that the mean and variance remains relatively constant over time, and that there was no systematic change in these metrics (i.e. no trend)]. We can _analyze and control_ for stationarity using time plots and statistical testing. If our series is not stationary, we may need to transform the data until it is. 

Once we have reliable baseline data, we can begin _hypothesis testing and model fitting_. During this step, we will produce a number of possible models and analyze the residuals to see if we have a good fit. 

Finally, once we have identified an adequate model, we can move on to _forecasting_. 

## An example time series

As an introduction to time series, let's examine a time series of 30-years of monthly data from Whitehorse, Yukon Territory. 

```{r, include = FALSE}
white <- read.csv("misc/1617-1971-2000-mly.csv")
library(ggplot2)
library(ggseas)
library(zoo)
white$Date <- as.yearmon(white$Date)
```

```{r}
ggplot(data = white) + geom_line(aes(Date, MeanTemp), colour = "red") + ylab("Mean Temperature (°C)") + ggtitle("Mean Temperature at Whitehorse, Yukon (1971-2000)") + scale_x_continuous()
```

We can decompose our time series into its components by the following equation: 

\[
\begin{aligned}
X_t &= T_t + S_t + R_t \\
\end{aligned}
\]

Where $X_t$ refers to the value of the observation at time $t$, $T_t$ is the tend component, $S_t$ is the seasonal component, and $R_t$ is the residual or random effect that isn't captured by our model. 

```{r}
ggsdc(na.contiguous(cbind(Date = white$Date, MeanTemp = white$MeanTemp)), aes(x = Date, y = MeanTemp), frequency = 12, method = "decompose") + geom_line() + xlab("Date") + ylab("Mean Temperature (°C)")
```

## Conclusions

Time series give us special insight into the drivers of systems such as the Earth's climate. The book you are reading will help you to analyze time series in R. In particular, we are looking at examples from climate science, but the skills you learn will be widely applicable across scientific disciplines. Before you can begin using R for advanced analysis, though, it is important that you review the material in Chapter \@ref(basics), which will familiarize you with R. If you are already an R used, you can skip to Chapter \@ref(desc_stats).
